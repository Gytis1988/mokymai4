import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.MobileCapabilityType;
import io.appium.java_client.touch.offset.PointOption;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

public class FirstTest extends SwipeScreen {
    public static AndroidDriver<MobileElement> driver;

    @BeforeSuite
    public void setupAppium() throws IOException {
        File classpathRoot = new File(System.getProperty("user.dir"));
        File appDir = new File(classpathRoot, "./src/test/java");
        File app = new File(appDir.getCanonicalPath(), "app-adax-release.apk");
        DesiredCapabilities capabilities = new DesiredCapabilities();

        capabilities.setCapability("app", app.getAbsolutePath());
        capabilities.setCapability(MobileCapabilityType.DEVICE_NAME, "Android Emulator");
        System.out.println(app.getAbsolutePath());

        driver = new AndroidDriver<MobileElement>(new URL("http://127.0.0.1:4723/wd/hub"), capabilities);
    }

    //5
    @AfterSuite
    public void uninstallApp() throws InterruptedException {
        // driver.removeApp("com.example.android.contactmanager");
    }

    //6
    @Test
    public void myFirstTest() {
        Dimension dims = driver.manage().window().getSize();
        int widthCenter = dims.width / 2;
        int heightCenter = dims.height / 2;
        driver.resetApp();
        super.swipeScreen(SwipeScreen.Direction.LEFT, driver);
        super.swipeScreen(SwipeScreen.Direction.LEFT, driver);
        TouchAction action = new TouchAction(driver);
        action.tap(PointOption.point(widthCenter, heightCenter).withCoordinates(widthCenter, heightCenter)).perform();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.findElementById("button_start_demo").click();
    }
}
